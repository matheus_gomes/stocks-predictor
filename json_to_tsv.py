# -*- encoding: UTF-8 -*-
import json
import argparse
import time

dicReverse = {-1: 'negative', 0: 'neutral', 1: 'positive'}

def convertToTsv(jsonIn, tsvOut, idxStart = 0, idxEnd = 0):
    with open(jsonIn) as dataIn:
        datasetIn = json.load(dataIn)

    if idxEnd == 0:
        idxEnd = len(datasetIn)

    fOut = open(tsvOut, 'w')

    dataLines = ''
    cont = int(time.time()) #inicializa um contador com o timestamp atual(apenas pra usar de índice de cada elemento do dataset) 
    for i in xrange(idxStart, idxEnd):
        dataLines= dataLines + ("%s\t%s\t%s %s\n"%(cont, dicReverse[datasetIn[i]['classification']], datasetIn[i]['headlineTitle'].encode('ascii', 'ignore'), datasetIn[i]['headlineText'].encode('ascii', 'ignore')))
        cont = cont + 1
    print(dataLines)
    fOut.write(dataLines)
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert json dataset to tsv dataset')
    parser.add_argument('-o', '--output', type=str, default= 'dataset.tsv', help='output(tsv)')
    parser.add_argument('-i', '--input', type=str, default= 'datasetEconomyNews_reuters+nytimes.json', help='out')
    convertToTsv(parser.parse_args().input, parser.parse_args().output)