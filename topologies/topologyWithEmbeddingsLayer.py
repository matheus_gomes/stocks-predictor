from keras.layers import Input, Dense, Embedding, Conv2D, MaxPool2D
from keras.layers import Reshape, Flatten, Dropout, Concatenate
from keras.callbacks import ModelCheckpoint
from keras.optimizers import Adam
from keras.models import Model

filter_sizes = [2,3,4]
drop = 0.5
num_filters = 16
#embed = FastText.load(os.path.join(path_to_embeddings, 'fast_text_300.model'))
#weights = embed.wv.vectors
#embedding_layer = Embedding(input_dim=weights.shape[0], output_dim=weights.shape[1], weights=[normalize(weights)], trainable=False)
#embedding_layer.input_lenght = sequence_length
class TopologyWithEmbeddingsLayer():
    def __init__(self, qtde_classes, sequence_length, embeddings):
        embedding_dim = embeddings.shape[1]
        
        input = Input(shape=(sequence_length,), dtype='int32')
        embedding = Embedding(input_dim = embeddings.shape[0], \
                            output_dim = embedding_dim, input_length = sequence_length, \
                            trainable = False, weights = [embeddings])(input)
        
        reshape = Reshape((sequence_length,embedding_dim,1))(embedding)

        conv_0 = Conv2D(num_filters, kernel_size=(filter_sizes[0], 300), padding='valid', kernel_initializer='normal', activation='relu')(reshape)
        conv_1 = Conv2D(num_filters, kernel_size=(filter_sizes[1], 300), padding='valid', kernel_initializer='normal', activation='relu')(reshape)
        conv_2 = Conv2D(num_filters, kernel_size=(filter_sizes[2], 300), padding='valid', kernel_initializer='normal', activation='relu')(reshape)
        
        maxpool_0 = MaxPool2D(pool_size=(sequence_length - filter_sizes[0] + 1, 1), strides=(1,1), padding='valid')(conv_0)
        maxpool_1 = MaxPool2D(pool_size=(sequence_length - filter_sizes[1] + 1, 1), strides=(1,1), padding='valid')(conv_1)
        maxpool_2 = MaxPool2D(pool_size=(sequence_length - filter_sizes[2] + 1, 1), strides=(1,1), padding='valid')(conv_2)

        concatenated_tensor = Concatenate(axis=1)([maxpool_0, maxpool_1, maxpool_2])
        flatten = Flatten()(concatenated_tensor)
        dropout = Dropout(drop)(flatten)
        output = Dense(units=qtde_classes, activation='softmax')(dropout)
        self.model = Model(inputs=input, outputs=output)
    def get_model(self):
        return self.model