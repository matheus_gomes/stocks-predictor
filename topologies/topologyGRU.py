import keras
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D, GRU, Dropout
from keras.models import Sequential

class TopologyGRU():
    def __init__(self, qntd_classes):
        self.model = Sequential()
        self.model.add(GRU(input_dim=(45, 300), output_dim=layers[1], activation='tanh', return_sequences=True))
        self.model.add(Dropout(0.15))
        self.model.add(Flatten())
        self.model.add(Dense(qntd_classes, activation='softmax',activity_regularizer=keras.regularizers.l2()))
    def get_model(self):
        return self.model