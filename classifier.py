#-*- encoding: utf-8 -*-
import keras, json, random, argparse
from keras.layers import Dense, Flatten
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D
from keras.models import Sequential
from keras.callbacks import Callback
# import matplotlib.pylab as plt
import numpy as np
from sklearn.model_selection import train_test_split, KFold
from sklearn import metrics

########## modules #############3
from topologies.topology1 import Topology1_2D
from topologies.textCNNtopology1 import TopologyTextCNN1
from topologies.textCNNtopology2 import TopologyTextCNN2
from topologies.textCNNtopology3 import TopologyTextCNN3
from topologies.textCNNtopology4 import TopologyTextCNN4

from metrics import Metrics

inspect = False #To inspect wrong predicted queries

sequenceLength = 80
embeddingDim = 300

class Classifier(object):
    dataset = []
    num_classes = 2
    x = []
    y = []
    y_test = []
    x_text = []
    model = None

    def __init__(self, datasetPath):
        with open(datasetPath) as f:
            self.dataset = json.load(f)
        self.num_classes = len(self.dataset[0]["classification"])
        self.model = TopologyTextCNN1(self.num_classes, sequenceLength, embeddingDim).get_model()
    
    def get_vec_mean(self, embedded_list):
        embedded_sum=0
        for emb in embedded_list:
            embedded_sum=embedded_sum+emb
        
        try:
            return embedded_sum/len(embedded_list)
        except ZeroDivisionError:
            return np.zeros(300)
    
    def reshape_matrix(self, lin, embedded_list):
        matrix=[]
        idx = 0
        while(len(matrix)<lin):
            if(idx > len(embedded_list)-1):
                matrix.append(np.zeros(300).tolist())    
            else:
                matrix.append(np.asarray(embedded_list[idx]).tolist())
            idx=idx+1
        
        if (idx < len(embedded_list)-1):
            matrix[lin-1]=self.get_vec_mean(np.asarray(embedded_list[(lin-1):])).tolist()

        return matrix

    def load_dataset2conv2D(self, dim):
        for block in self.dataset:
            raw_matrix = self.reshape_matrix(dim[0], block["embeddedsMatrix"])
            nparray=np.asarray(raw_matrix)
            nparray=np.reshape(nparray, (dim[0], dim[1], 1))
            self.x.append(nparray)
            nparray=np.asarray(block["classification"])
            self.y.append(nparray)
        
        #self.x, self.x_test, self.y, self.y_test = train_test_split(self.x, self.y, test_size=0.20, random_state=seed)
        #print("#DATASET STATISTICS\n")
        #print("x_train + validation size= "+str(len(self.x)))
        #print("x_test size= "+str(len(self.x_test)))
        #print("#train+validation batch statistics")
        #self.datasetStatistics(self.y)
        #print("#test batch statistics")
        #self.datasetStatistics(self.y_test)

        self.x = np.asarray(self.x)
        self.y = np.asarray(self.y)
        #self.x_test = np.asarray(self.x_test)
        #self.y_test = np.asarray(self.y_test)
    
    def run_model(self):
        def fmap(y):
            return 1 if y[1] == 1 else -1
        
        self.model.summary()
        
        y_scores = []
        
        #optimizer = keras.optimizers.SGD(lr=0.01, decay=1e-5, momentum=0.9, nesterov=True)
        optimizer = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
        #optimizer = keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
        nfold = 0
        kf = KFold(n_splits= 10)
        for train, test in kf.split(self.x):
            nfold = nfold + 1
            self.model = TopologyTextCNN1(self.num_classes, sequenceLength, embeddingDim).get_model()
            #print("%s %s" % (train, test))
            x_train, x_test, y_train, y_test = self.x[train], self.x[test], self.y[train], self.y[test]

            self.model.compile(loss=keras.losses.categorical_crossentropy,
                optimizer=optimizer,
                metrics=['accuracy', Metrics().precision, Metrics().recall, Metrics().f1])
            self.model.fit(x_train, y_train, batch_size=32, epochs=epochs, verbose=0)

            score = self.model.evaluate(x_test, y_test, batch_size=32, verbose=1)

            y_labels = np.asarray(map(fmap, y_test))
            y_score_fold = []
            
            for sc in self.model.predict(x_test):
                y_score_fold.append(sc[1])
                y_scores.append(sc[1]) 
            
            def round(y):
                return 1 if y >= 0.5 else -1

            acc = metrics.accuracy_score(y_labels, map(round, y_score_fold))
            precision = metrics.precision_score(y_labels, map(round, y_score_fold), pos_label=1)
            recall = metrics.recall_score(y_labels, map(round, y_score_fold), pos_label=1)
            f1 = metrics.f1_score(y_labels, map(round, y_score_fold), pos_label=1)
            print('\n--------------------------------------------- TESTE ----------------------------------------------')
            print('FOLD%d %s = %f, %s = %f, %s = %f, %s = %f' %(nfold, 'accuracy', acc, 'precision', precision, 'recall', recall, 'f1', f1))
        
        y_labels = np.asarray(map(fmap, self.y))
        y_scores = np.array(y_scores)
        fpr, tpr, thresholds = metrics.roc_curve(y_labels, y_scores, pos_label=1)
        auc = metrics.auc(fpr, tpr)
        print("fpr: ", fpr.tolist())
        print("tpr: ", tpr.tolist())
        print("thresholds: ", thresholds.tolist())
        print("auc: ", auc)
        with open("roc_auc.json", "w") as f:
            f.write(json.dumps({"fpr": fpr.tolist(), "tpr": tpr.tolist(), "thresholds": thresholds.tolist(), "auc": auc, "y_real": self.y.tolist(), "y_score": y_scores.tolist()}, indent=4, separators=(',', ': ')))

    def datasetStatistics(self, y):
        neutral=0
        negative=0
        positive=0

        for i in y:
            if(i[0] == 1):
                negative = negative + 1
            elif(i[1] == 1):
                neutral = neutral + 1
            else:
                positive = positive + 1
        total = neutral + negative + positive
        print("positives:%f    negatives:%f    neutral:%f"%(float(positive)/total, float(negative)/total, float(neutral)/total)) 

    def query_predict_2d(self, dim):
        n=0
        n_acc=0
        n_err=0
        index2class = {
            0: [1, 0, 0],
            1: [0, 1, 0],
            2: [0, 0, 1]
        }
        #busca query no dicionario intent_dict
        for block in self.dataset:
            n=n+1
            raw_matrix = self.reshape_matrix(dim[0], block["embeddedsMatrix"])
            nparray=np.asarray(raw_matrix)
            pred_array=np.reshape(nparray, (1, dim[0], dim[1], 1))
            prediction=self.model.predict(pred_array , batch_size=1)
            predict = prediction[0]
            predict = predict.tolist()
            indx=predict.index(max(predict)) #retorna o índice do maior elemento do vetor predict
            
            if(index2class[indx] != block["classification"]):
                    try:
                        print("#falha:\n\tnews = ",block["news"],"\n\tclassification = ", block["classification"], "\n\tpredicted = ", index2class[indx])
                    except UnicodeEncodeError:
                        print("## UNICODE ENCODE ERROR")
                    n_err = n_err+1
            else:
                    n_acc = n_acc+1
            #print('\npredict index= %d , predict intent= %s'%(indx, index2class[indx])+'\nvec_prediction= '+str(predict))
            #print("gabarito = ",block["label"], " intent = ", block["intent"])
        print("## numero acertos = ", n_acc)
        print("## numero erros = ", n_err)
        print("## taxa acc = ", float(n_acc)/n)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plataform for test in convolutional neural networks')
    parser.add_argument('-s', '--seed', type=int, default= 42, help='seed for random generator')
    parser.add_argument('-d', '--dataset', type=str, default= 'datasetEmbedding.json', help='dataset path')
    parser.add_argument('-e', '--epochs', type=int, default= 5, help='number of epochs to fit the model')
    parser.add_argument('-i', '--inspect', action='store_true', default= False)
    epochs = parser.parse_args().epochs
    seed = parser.parse_args().seed
    datasetPath = parser.parse_args().dataset
    inspect = parser.parse_args().inspect

    classifier = Classifier(datasetPath)
    classifier.load_dataset2conv2D((sequenceLength, embeddingDim))
    classifier.run_model()

    if (inspect):
        classifier.query_predict_2d((sequenceLength, embeddingDim))