#-*- ecoding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
import argparse
import time, json, logging
from datetime import datetime

parser = argparse.ArgumentParser(description='crawler to get news and generate a dataset from it.')
parser.add_argument('--out', type=str, default= 'datasetEconomyNews.json', help='name of out file')
parser.add_argument('--log', type=str, default= 'log_webcrawler.log', help='name of log file')

jsonOutName = parser.parse_args().out
LOG_FILENAME = parser.parse_args().log

logging.basicConfig(filename=LOG_FILENAME, level= logging.DEBUG)

class Crawler(object):
    latestNytimes = ''
    latestReuters = ''
    latestGoogle = ''

    dataset = []
    jsonOut = None
    browser = None
    def __init__(self, jsonOut, browser):
        self.jsonOut = jsonOut
        self.browser = browser
        try:
            with open(jsonOut, 'r') as f:
                self.dataset = json.load(f)
        except IOError:
            logging.info("Dataset File not exists, opening new file " + jsonOut)
            with open(jsonOut, 'w') as f:
                f.write(json.dumps(self.dataset))
    
    def crawlingGoogle(self):
        try:
            self.browser.get('https://www.google.com/search?q=shares,+stocks,+economy,+business&tbm=nws&source=lnt&tbs=qdr:h&sa=X&ved=0ahUKEwipgs-yiLDhAhW0H7kGHRYgCioQpwUIHw&biw=1366&bih=637&dpr=1')

            headlineTitle = []
            headlineText = []
            journal = []

            newsList = self.browser.find_elements_by_class_name('g')
            # para cada notícia
            for news in newsList:
                textsList = news.text.split("\n")
                headlineTitle.append(textsList[0].replace('...', '. '))
                journal.append(textsList[1])
                headlineText.append(textsList[2].replace('...', '. '))
            
            for i in range(len(headlineText)):
                if (headlineTitle[i] != self.latestGoogle and self.isNotInDataset(headlineTitle[i])):
                    logging.info("appending ("+journal[i]+") news in dataset. Title = " + headlineTitle[i])
                    self.dataset.append(dict(timestamp= str(time.time()), datetime = str(datetime.now()), journal=journal[i], headlineTitle=headlineTitle[i], headlineText=headlineText[i], classification=[]))
                    self.latestGoogle = headlineTitle[i]
            
            self.writeData()
        except:
            logging.info('Erro no crawler Google') 


    def crawlingNytimes(self):
        try:
            self.browser.get('https://www.nytimes.com/section/business/economy')

            headlineTitle = self.browser.find_elements_by_class_name("e1xfvim30")
            headlineText = self.browser.find_elements_by_class_name("e1xfvim31")

            for i in range(len(headlineText)):
                if (headlineTitle[i].text != self.latestNytimes and self.isNotInDataset(headlineTitle[i].text)):
                    logging.info("appending nytimes news in dataset. Title = " + headlineTitle[i].text)
                    self.dataset.append(dict(timestamp= str(time.time()), datetime = str(datetime.now()), journal="nytimes", headlineTitle=headlineTitle[i].text, headlineText=headlineText[i].text, classification=[]))
                    self.latestNytimes = headlineTitle[i].text
            self.writeData()
        except:
            logging.info('Erro no crawler NyTimes') 

    def crawlingReuters(self):
        try:
            self.browser.get('https://www.reuters.com/news/archive/businessnews?view=page&page=1&pageSize=10')
            
            headlineTitle = []
            headlineText = []
    
            pageText = self.browser.find_element_by_class_name("news-headline-list")

            cont = 0
            for text in pageText.text.split('\n'):
                # fazer ciclo de 3 elementos (título, manchete e horário)
                if (cont % 3 == 0):
                    cont = 0
                cont = cont + 1
                if cont == 1: 
                    headlineTitle.append(text)
                if cont == 2:
                    headlineText.append(text)
            
            for i in range(len(headlineText)):
                if (headlineTitle[i] != self.latestReuters and self.isNotInDataset(headlineTitle[i])):
                    logging.info("appending reuters news in dataset. Title = " + headlineTitle[i])
                    self.dataset.append(dict(timestamp= str(time.time()), datetime = str(datetime.now()), journal="reuters", headlineTitle=headlineTitle[i], headlineText=headlineText[i], classification=[]))
                    self.latestReuters = headlineTitle[i]
            
            self.writeData()

        except IndexError:
            logging.info('Falha ao parsear notícia reuters')
        except:
            logging.info('Erro no crawler reuters')
            
    def writeData(self):
        with open(self.jsonOut, 'w') as fOut:
            fOut.write(json.dumps(self.dataset, sort_keys=True, indent=4, separators=(',', ': ')))

    def isNotInDataset(self, title):
        for block in self.dataset:
            if (block["headlineTitle"] == title):
                return False
        return True
        
if __name__ == '__main__':
    crawler = Crawler(jsonOutName, webdriver.PhantomJS())
    
    while(True):
        crawler.crawlingNytimes()
        crawler.crawlingReuters()
        crawler.crawlingGoogle()
        #aguarda 10 minutos
        time.sleep(10*60)
