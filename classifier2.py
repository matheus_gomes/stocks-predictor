import keras, json, random, argparse
from keras.layers import Dense, Flatten
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D
from keras.models import Sequential
from keras.callbacks import Callback
# import matplotlib.pylab as plt
import numpy as np
from sklearn.model_selection import train_test_split

########## modules #############3
from topologies.topologyWithEmbeddingsLayer import TopologyWithEmbeddingsLayer

from metrics import Metrics

sequenceLength = 60
embeddingDim = 300

class Classifier(object):
    dataset = []
    num_classes = 3
    x = []
    y = []
    y_test = []
    x_text = []

    def __init__(self, datasetPath):
        with open(datasetPath) as f:
            self.dataset = json.load(f)
        self.num_classes = len(self.dataset[0]["classification"])
    
    def get_vec_mean(self, embedded_list):
        embedded_sum=0
        for emb in embedded_list:
            embedded_sum=embedded_sum+emb
        
        try:
            return embedded_sum/len(embedded_list)
        except ZeroDivisionError:
            return np.zeros(300)
    
    def reshape_matrix(self, lin, embedded_list):
        matrix=[]
        idx = 0
        while(len(matrix)<lin):
            if(idx > len(embedded_list)-1):
                matrix.append(np.zeros(300).tolist())    
            else:
                matrix.append(np.asarray(embedded_list[idx]).tolist())
            idx=idx+1
        
        if (idx < len(embedded_list)-1):
            matrix[lin-1]=self.get_vec_mean(np.asarray(embedded_list[(lin-1):])).tolist()

        return matrix

    def load_dataset2conv2D(self, dim):
        for block in self.dataset:
            raw_matrix = self.reshape_matrix(dim[0], block["embeddedsMatrix"])
            nparray=np.asarray(raw_matrix)
            nparray=np.reshape(nparray, (dim[0], dim[1], 1))
            self.x.append(nparray)
            nparray=np.asarray(block["classification"])
            self.y.append(nparray)
        
        self.x, self.x_test, self.y, self.y_test = train_test_split(self.x, self.y, test_size=0.33, random_state=seed)
        print("#DATASET STATISTICS\n")
        print("x_train + validation size= "+str(len(self.x)))
        print("x_test size= "+str(len(self.x_test)))
        print("#train+validation batch statistics")
        self.datasetStatistics(self.y)
        print("#test batch statistics")
        self.datasetStatistics(self.y_test)

        self.x = np.asarray(self.x)
        self.y = np.asarray(self.y)
        self.x_test = np.asarray(self.x_test)
        self.y_test = np.asarray(self.y_test)
    
    def run_model(self):
        model = TopologyWithEmbeddingsLayer(self.num_classes, sequenceLength, embeddings).get_model()
        model.summary()
        
        #optimizer = keras.optimizers.SGD(lr=0.01, decay=1e-5, momentum=0.9, nesterov=True)
        #optimizer = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
        optimizer = keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)

        model.compile(loss=keras.losses.categorical_crossentropy,
            optimizer=optimizer,
            metrics=['accuracy', Metrics().f1])
        model.fit(self.x, self.y, batch_size=32, epochs=epochs, validation_split=0.3, shuffle=True)

        score = model.evaluate(self.x_test, self.y_test, batch_size=32, verbose=1)

        print('\nteste')
        print('%s = %f , %s = %f, %s = %f' %(model.metrics_names[0], score[0], model.metrics_names[1], score[1], model.metrics_names[2], score[2]))
    
    def datasetStatistics(self, y):
        neutral=0
        negative=0
        positive=0

        for i in y:
            if(i[0] == 1):
                negative = negative + 1
            elif(i[1] == 1):
                neutral = neutral + 1
            else:
                positive = positive + 1
        total = neutral + negative + positive
        print("positives:%f    negatives:%f    neutral:%f"%(float(positive)/total, float(negative)/total, float(neutral)/total)) 


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plataform for test in convolutional neural networks')
    parser.add_argument('-s', '--seed', type=int, default= 42, help='seed for random generator')
    parser.add_argument('-d', '--dataset', type=str, default= 'datasetEmbedding.json', help='dataset path')
    parser.add_argument('-e', '--epochs', type=int, default= 5, help='number of epochs to fit the model')

    epochs = parser.parse_args().epochs
    seed = parser.parse_args().seed
    datasetPath = parser.parse_args().dataset

    classifier = Classifier(datasetPath)
    classifier.load_dataset2conv2D((sequenceLength, embeddingDim))
    classifier.run_model()