#-*- ecoding: UTF-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
import argparse
import time, json

parser = argparse.ArgumentParser(description='crawler to get news and generate a dataset from it.')
parser.add_argument('--out', type=str, default= 'datasetEconomyNews(reuters)_.json', help='name of out file')
jsonOutName = parser.parse_args().out

def crawling_nytimes():
    browser = webdriver.Chrome()
    browser.get('https://www.nytimes.com/section/business/economy')

    #aguarda 10 segundos
    for i in range(0, 29):
        time.sleep(3)
        browser.find_element_by_xpath('//*[@id="stream-panel"]/div[1]/div/div/div/button').click()

    time.sleep(5)

    headlineTitle = browser.find_elements_by_class_name("e1xfvim30")
    headlineText = browser.find_elements_by_class_name("e1xfvim31")
    
    dataset=[]

    for i in range(len(headlineText)):
        print("# "+headlineTitle[i].text)
        print("--> "+headlineText[i].text)
        print("\n")
        dataset.append(dict(idx=i,headlineTitle=headlineTitle[i].text, headlineText=headlineText[i].text, classification=[]))
    generateJson(dataset)

# story-title
def crawling_routers(pageStart, pageEnd):
    browser = webdriver.Chrome()
    
    headlineTitle = []
    headlineText = []
    headlineHour = []
    
    for i in xrange(pageStart, pageEnd):
        browser.get('https://www.reuters.com/news/archive/businessnews?view=page&page=%d&pageSize=10' %(i))
        time.sleep(5)
        pageText = browser.find_element_by_class_name("news-headline-list")
        
        cont = 0
        for text in pageText.text.split('\n'):
            # fazer ciclo de 3 elementos (título, manchete e horário)
            if (cont % 3 == 0):
                cont = 0
            cont = cont + 1

            if (cont == 1): 
                headlineTitle.append(text)
            elif cont == 2:
                headlineText.append(text)
            else:
                headlineHour.append(text)
    
    dataset = []
    for i in range(len(headlineText)):
        print("# "+headlineTitle[i])
        print("--> "+headlineText[i])
        print("\n")
        dataset.append(dict(idx=i+100,headlineTitle=headlineTitle[i], headlineText=headlineText[i], hour=headlineHour[i], classification=[]))
    generateJson(dataset)

def generateJson(dataset):
    with open(jsonOutName, 'w') as datasetFile:
        datasetFile.write(json.dumps(dataset, sort_keys=True, indent=4, separators=(',', ': ')))

if __name__ == '__main__':
    #crawling_nytimes()
    crawling_routers(2000, 2002)