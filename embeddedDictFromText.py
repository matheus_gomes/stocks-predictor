#-*- encoding: UTF-8 -*-
import json
import os, io 
import numpy as np
import gensim
import argparse
from pprint import pprint

classToHotVec = {
    -1: [1, 0, 0],
    0: [0, 1, 0],
    1: [0, 0, 1]
}

parser = argparse.ArgumentParser(description='dictionary generator to join news with your respective embeddeds')
parser.add_argument('--newsDataSet', type=str, default='datasetEconomyNews.json', help='dataset of news and your classification (bad, neutral, good)')
parser.add_argument('-e', '--embeddeds', type=str, default='../GoogleNews-vectors-negative300.bin.gz', help='name of embeddeds dataset')
parser.add_argument('-o', '--out', type=str, default='datasetEmbedding.json', help='generated dataset with embeddeds')

dataSetPath = parser.parse_args().newsDataSet
embeddedsPath = parser.parse_args().embeddeds
jsonOutPath = parser.parse_args().out

jsonData = []
embeddings = {}

def openEmbeddedsGoogleNews(embeddedsPath):
    return gensim.models.KeyedVectors.load_word2vec_format(embeddedsPath, binary=True) 

def openEmbeddedsGlove(gloveFile):
    print "Loading Glove Model"
    f = open(gloveFile,'r')
    model = {}
    for line in f:
        splitLine = line.split()
        word = splitLine[0]
        embedding = np.array([float(val) for val in splitLine[1:]])
        model[word] = embedding
    print "Done.",len(model)," words loaded!"
    return model

def openEmbeddedsFastText(embeddedsPath):
    return gensim.models.KeyedVectors.load_word2vec_format(embeddedsPath)

def openDataSet(dataSetPath):
    with open(dataSetPath, 'r') as f:
        return json.load(f)

def getMeanOfWords(jsonData):
    cont = 0
    totalLen = 0
    for item in jsonData:
        cont = cont + 1
        lenSubText = len(item['headlineText'].split(' '))
        lenTitle = len(item['headlineTitle'].split(' '))
        totalLen = totalLen + lenTitle + lenSubText
	
    return totalLen/cont

def getMaxOfWords(jsonData):
    maxOfWords = 0
    for item in jsonData:
        lenSubText = len(item['headlineText'].split(' '))
        lenTitle  = len(item['headlineTitle'].split(' '))
        
        if(maxOfWords < (lenTitle + lenSubText)):
            maxOfWords = (lenTitle + lenSubText)
	
    return maxOfWords

def word2embedded(query):
    especialCharacters = ['?', ',', ':', ')', '(', '.', '\n', "'s"]
    embedded_list=[]
    for word in query.split(' '):
        try:
            clean_word = word
            for charEsp in especialCharacters:
                clean_word = clean_word.replace(charEsp, '')
            clean_word = clean_word.encode('ascii', 'ignore')
            print('\t\t'+clean_word)
        except:
            print("It has an issue in replace method")
        #adicionando cada embedded referente a uma palavra a uma lista de embeddeds 
        try:
            embedded_list.append(embeddings[clean_word])
        except KeyError:
            print("word \"%s\" not in vocabulary" %(clean_word))

    return embedded_list

def get_vec_mean(embedded_list):
    embedded_sum=0
    for emb in embedded_list:
        embedded_sum=embedded_sum+emb
    
    try:
        return embedded_sum/len(embedded_list)
    except ZeroDivisionError:
        return np.zeros(300)

def matrix_formatter(embedded_list):
    matrix = []
    
    for lin in embedded_list:
        #para caso não for numpyarray não dar erro na hra de converter pra lista, então forço a ser numpyarray, e caso for numpyarray vai converter pra lista do mesmo jeito
        matrix.append(np.asarray(lin).tolist())
    return matrix

def buildingNewsDataSet(jsonData, embeddings):
    dataset = []
    for item in jsonData:
        resultNews = item['headlineTitle'] + ' ' +item['headlineText']
	
        embedded_list = word2embedded(resultNews)
        
        try:
            vecMean = get_vec_mean(embedded_list)
        except TypeError:
            vecMean = get_vec_mean(np.asarray(embedded_list))

        dataset.append(dict(news =  resultNews, classification = classToHotVec[item['classification']], meanEmbedded=vecMean.tolist(), embeddedsMatrix=matrix_formatter(embedded_list)))
    return dataset

if __name__ == "__main__":
    jsonData = openDataSet(dataSetPath)
    print('media de palavras: %d' %(getMeanOfWords(jsonData)))
    print('máximo de palavras: %d' %(getMaxOfWords(jsonData)))
    embeddings = openEmbeddedsGoogleNews(embeddedsPath) #Para os embeddings do GoogleNews WordVectors
    #embeddings = openEmbeddedsFastText(embeddedsPath) #Para embeddings do fastText
    #embeddings = openEmbeddedsGlove(embeddedsPath)
    datasetOut = buildingNewsDataSet(jsonData, embeddings)
    
    del embeddings

    with open(jsonOutPath, 'w') as embclassFile:
        embclassFile.write(json.dumps(datasetOut))
